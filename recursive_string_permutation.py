#!/usr/bin/env python

from __future__ import print_function

def get_permutation(mystr):
    # base case
    if len(mystr) <= 1:
        return [mystr]
    # recursion
    else:
        ret = []
        for i in range(0, len(mystr)):
            ret += [
                    item[:i] + mystr[0] + item[i:] 
                    for item in get_permutation(mystr[1:])
                    ]
        return list(set(ret))

# test case
if __name__ == '__main__':
    print(get_permutation("bust"))